const low = require('lowdb');

const db  = low('db.json');

function DBManager(){}

DBManager.prototype.initializeDB = function () {
  // Set some defaults if your JSON file is empty
  db.defaults({ folders: [], playlists: [] }).write();
};

/**
* @param {PlayList} playlist
*/
DBManager.prototype.updatePlayList = function (playlist) {
  db.get('playlists')
  .find({ name: playlist.name })
  .assign({ songs: playlist.songs})
  .write();
};

/**
* @param {Folder} folder
*/
DBManager.prototype.addFolder = function (folder) {
  db.get('folders')
  .push(folder)
  .write();
};

/**
* @param {PlayList} playlist
*/
DBManager.prototype.addPlayList = function (playlist) {
  db.get('playlists')
  .push(playlist)
  .write();
};

/**
* @return {Array<Folder>} an array of playlists that contain song objects
*/
DBManager.prototype.getFolders = function () {
  return db.get('folders').value();
};

/**
* @return {Array<PlayList>} an array of playlists that contain song objects
*/
DBManager.prototype.getPlayLists = function () {
  return db.get('playlists').value();
};

DBManager.prototype.removeDuplicate = function (folder) {
  db.get('folders').remove(folder).write();
};

module.exports = DBManager;
