const Folder    = require("../models/folder.js");
const Song      = require("../models/song.js");
const PlayList  = require("../models/playlist.js");
const f         = require("../lib/functions.js");

function SongManager(){}

/**
* @param {Array<PlayList>} playlists
* @return {Array<PlayList>} an array of playlists that contain song objects
*/
SongManager.prototype.deserializePlaylists = function(playlists){
  let dis = this;
  return playlists.map(function(playlist){
    return dis.deserializePlaylist(playlist);
  });
};

/**
* @param {PlayList} playlist
* @return {PlayList} a playlist with song objects
*/
SongManager.prototype.deserializePlaylist = function(playlist){
  let dis   = this;
  let songs = playlist.songs.map(function(songPath){
    return dis.deserializeSong(songPath);
  });

  let dPlaylist   = new PlayList(playlist.name);
  dPlaylist.setSongs(songs);

  return dPlaylist;
};

/**
* @param {String} songPath
* @return {Song} a song object with a name and folder object
*/
SongManager.prototype.deserializeSong = function(songPath){
  return new Song(songPath);
};

/**
* @param {PlayList} playlist
* @return {PlayList} a playlist with songpaths
*/
SongManager.prototype.serializePlaylist = function(playlist){
  let dis = this;
  let songs = playlist.songs.map(function(song){
    return dis.serializeSong(song);
  });
  let dPlaylist   = new PlayList(playlist.name);
  dPlaylist.setSongs(songs);

  return dPlaylist;
};

/**
* @param {Song} song
* @return {String} a songPath
*/
SongManager.prototype.serializeSong = function(song){
  return song.path;
};

module.exports = SongManager;
