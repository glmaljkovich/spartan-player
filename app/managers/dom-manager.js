const f = require('../lib/functions.js');

function DOMManager(){}

// -----------------------------------------------------
//  Player
// -----------------------------------------------------

DOMManager.prototype.registerUpdateLoop = function() {
  let dis = this;
  song.addEventListener('timeupdate',function (){
      if(!moving){
        var currentTime = parseInt(song.currentTime, 10);
        $("#playing-time").val(currentTime);
        $('#position').text(dis.toPlayTime(currentTime));
        $('#playing-time').change();
      }
  });
};

/**
* @param {Audio} audio
*/
DOMManager.prototype.initializeSlider = function(audio) {
  slider = new Foundation.Slider($('#slider'), {
    'binding': true,
    'changedDelay': 0,
    'dataMoveTime': 0,
    'end': parseInt(audio.duration, 10)
  });
};

/**
* @param {Song} song
*/
DOMManager.prototype.loadMetadata = function(song) {
  let dis = this;
  $(song.audio).on('loadedmetadata', function(){
    // Set duration
    $('#duration').text(dis.toPlayTime(song.audio.duration));
    // Initialize the slider
    dis.initializeSlider(song.audio);
    // Set Metadata
    dis.applySongMetadata(song.path);
  });
};

/**
* @param {String} songPath
*/
DOMManager.prototype.applySongMetadata = function(songPath){
  let dis = this;
  jsmediatags.read(songPath, {
    onSuccess: function(tag) {
      var tags    = tag.tags;
      var image   = tags.picture;
      dis.setCoverImage(dis.processBase64Image(image));
      if (tags.title === undefined) {
        $('.song-name').text(f.getFileName(songPath));
      }else{
        $('.song-name').text(tags.title);
      }
      $('.album-data').text(tags.artist + " - " + tags.album);

    },
    onError: function(error) {
      console.log(':(', error.type, error.info);
    }
  });
};

/**
* Track the slider position and update the player accordingly
* @param {Audio} audio
*/
DOMManager.prototype.trackSliderPosition = function(audio){
  let that = this;
  $('#marker').mousedown(function(e){
    moving = true;
    console.log("start moving");
  });

  $('#marker').mousemove(function(e){
    $('#position').text(that.toPlayTime(that.getCurrentSeconds()));
  });

  $('#marker').mouseup(function(e){
    // Update song currentTime
    audio.currentTime = that.getCurrentSeconds();
    // Update playing time
    $('#position').text(that.toPlayTime(that.getCurrentSeconds()));
    moving = false;
  });
};


// -----------------------------------------------------
//  Time
// -----------------------------------------------------

/**
* Makes sure the time is displayed as a two ciphers number
* @param {int} duration
* @return {String} _ two character time
*/
DOMManager.prototype.normalizeTime = function (duration){
  if(duration < 10){
    return '0' + duration + '';
  }else{
    return duration + '';
  }
};

/**
* Takes a duration in seconds and converts it to a playtime.
* @param {int} seconds
* @return string with a readable playtime
*/
DOMManager.prototype.toPlayTime = function (seconds){
  var time    = moment.duration(seconds, 'seconds');
  return(this.normalizeTime(time.minutes()) + ':' + this.normalizeTime(time.seconds()));
};

DOMManager.prototype.getCurrentSeconds = function (){
  return $('#playing-time').val()/1;
};


// -----------------------------------------------------
// 1. Image Processing
// -----------------------------------------------------

DOMManager.prototype.generateImageCover = function (canvasId, imgPath){
  canvas        = document.getElementById(canvasId);
  canvas.height = $(window).height();
  canvas.width  = $(window).width();
  ctx           = canvas.getContext('2d');
  var img       = new Image();
  img.src       = imgPath;
  // function call
  img.onload    = this.draw;
};

/**
* Sets the cover image in all three locations
*/
DOMManager.prototype.setCoverImage = function (dataUrl){
  var url = dataUrl;
  if(url === ""){
    url = "img/cover.png";
  }
  $('#album-cover-small').attr('src', url);
  $('#album-cover').attr('src', url);
  // Fill the background with a blurred image
  this.generateImageCover('cover-background', url);
};

/**
* Called by #generateImageCover
**/
DOMManager.prototype.draw = function () {
    drawImageProp(ctx, this);
    StackBlur.canvasRGB(document.getElementById('cover-background'), 0, 0, canvas.width, canvas.height, 15);
};

/**
* @return a base64 url
**/
DOMManager.prototype.processBase64Image = function (image){
  if (image === undefined) {
    return "";
  }
  var base64String = "";
  for (var i = 0; i < image.data.length; i++) {
      base64String += String.fromCharCode(image.data[i]);
  }
  return "data:" + image.format + ";base64," + window.btoa(base64String);
};

module.exports = DOMManager;
