// At the top of the file to make them available to all modules
var canvas, ctx, song, playing, slider, moving,
playing = false;
moving  = false;

let $           = require('jquery');
let jQuery      = require('jquery');
window.$        = $;

let jsmediatags = require("jsmediatags");

require('foundation-sites');

const shell     = require('electron').shell;
const os        = require('os');
const ipc       = require('electron').ipcRenderer;
const dialog    = require('electron').remote.dialog;
const fs        = require("fs");

const AppComponent          = require('./app/components/app-component.js');
const FolderComponent       = require('./app/components/folder-component.js');
const SidebarComponent      = require('./app/components/sidebar-component.js');
const SongComponent         = require('./app/components/song-component.js');
const AddMenuComponent      = require('./app/components/add-menu-component.js');
const AddPlayListComponent  = require('./app/components/add-playlist-component.js');
const PlayerComponent       = require('./app/components/player-component.js');
const PlayListComponent     = require('./app/components/playlist-component.js');
const CoverComponent        = require('./app/components/cover-component.js');

let dummySong = './app/song.mp3';


//--- Calls ------------------------------

let app = new AppComponent();
app.$mount('#overlay');

// Initialize Foundation
$(document).foundation();
