
// -----------------------------------------------------
//   FUNCTIONS
// -----------------------------------------------------
// 1. Image Processing
// 2. Time
// 3. Player
// -----------------------------------------------------


// -----------------------------------------------------
// 5. Files
// -----------------------------------------------------

exports.getFileName = function (path){
  return path.replace(/^.*[\\\/]/, '');
};

exports.getFolderPath = function (filePath){
  return filePath.substring(0, filePath.lastIndexOf("/"));
};

exports.isMP3 = function (filename){
  return filename.endsWith('.mp3');
};

exports.remove = function (array, element) {
    const index = array.indexOf(element);

    if (index !== -1) {
        array.splice(index, 1);
    }
};
