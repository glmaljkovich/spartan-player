const Node        = require('./linkedlist.js').Node;
const LinkedList  = require('./linkedlist.js').LinkedList;
const Song        = require('./song.js');

function SongList(songArray){
  this.originalOrder  = songArray;
  this.list           = this.getNodeArray(songArray);
}

/**
* Returns an Array<Node> with the songs connected in the order of @array
* @param {Array<Song>} array
* @return {Array} array
*/
SongList.prototype.getNodeArray = function(array){
  let nodes   = new LinkedList();
  return array.map(function(song){
      return nodes.add(song);
  });
};

SongList.prototype.from = function(array){
  this.originalOrder  = array;
  this.list           = this.getNodeArray(array);
};

/**
* Reorders the list
*/
SongList.prototype.shuffle = function(){
  let array = this.originalOrder.slice();

  for (let i = array.length; i; i--) {
        let j = Math.floor(Math.random() * i);
        [array[i - 1], array[j]] = [array[j], array[i - 1]];
  }
  this.list = this.getNodeArray(array);
};

/**
* Reorders the list
*/
SongList.prototype.setOriginalOrder = function(){
  this.list = this.getNodeArray(this.originalOrder);
};

/**
* Returns a Node containing the specified song
* @param {Song} song
* @return {Node}
*/
SongList.prototype.findSong = function(song){
  let result = this.list.find(function(node){
    return (node.data.name == song.name) && node.data.path == song.path;
  });

  if(result){
    return result;
  }
  return null;
};

/**
* @param {Song} song
* @return {Song}
*/
SongList.prototype.nextSong = function(song){
  let next = this.findSong(song).next;
  if(next === null){
    return next;
  }
  return next.data;
};

/**
* @param {Song} song
* @return {Song}
*/
SongList.prototype.previousSong = function(song){
  let prev = this.findSong(song).previous;
  if(prev === null){
    return prev;
  }
  return prev.data;
};

module.exports = SongList;
