const f = require('../lib/functions.js');

function Song(songPath){
  this.name       = f.getFileName(songPath);
  this.path       = songPath;
  this.selected   = false;
  this.audio      = null;
}

module.exports = Song;
