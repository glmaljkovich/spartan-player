function PlayList(name){
  this.name   = name;
  this.songs  = [];
}

PlayList.prototype.setSongs = function(songs){
  this.songs = songs;
};

module.exports = PlayList;
