const f = require("../lib/functions.js");

function Folder(path){
  this.name = f.getFileName(path);
  this.path = path;
}

module.exports = Folder;
