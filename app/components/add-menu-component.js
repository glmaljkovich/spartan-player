Vue.component('add-menu', {
  template: `
  <div class="add-menu text-center">
    <a id="addFolder" href="#" v-on:click="showSelectFolderDialog">+Folder</a>
    <a data-open="add-playlist">+Playlist</a>
  </div>`,
  mounted: function(){
    this.registerAddFolderButton();
  },
  methods: {
    registerAddFolderButton: function() {
      let dis = this;
      ipc.on('selected-directory', function (event, folders) {
        // Logging...
        console.log(folders[0]);

        let folder = new Folder(folders[0]);
        dis.$emit('add-folder', folder);
      });
    },
    showSelectFolderDialog: function() {
      ipc.send('open-folder-dialog');
    }
  }
});
