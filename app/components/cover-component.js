Vue.component('cover', {
  template: `
  <div class="small-12 columns off-canvas-content" id="cover-container" data-off-canvas-content>
      <a href="#" id="menu-button" data-toggle="toolbar"><i class="fa fa-bars fa-lg" aria-hidden="true" ></i></a>
        <div v-if="currentSong" class="small-10 columns" id="metadata">
          <img src="img/cover.png" alt="" id="album-cover"/>
          <div class="right">
            <h1 class="song-name">--</h1>
            <h4 class="album-data">--</h4>
          </div><!-- /.right-->
        </div><!-- /#metadata-->
        <div v-else class="text-center float-left welcome">
          <h1 class="float-center" style="margin: 5rem auto;">Welcome</h1>
        </div>
  </div><!-- /#cover-container-->`,
  props: ['currentSong']
});
