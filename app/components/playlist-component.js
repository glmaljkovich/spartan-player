const Song    = require('../models/song.js');
const f       = require('../lib/functions.js');
const Folder  = require('../models/folder.js');

Vue.component('playlist', {
  template: `<li class="song-container">
    <a href="#" class="playlist">
      {{playlist.name}}
      <i class="fa fa-plus-circle add-button" aria-hidden="true" v-on:click="showAddSongDialog()"></i>
    </a>
    <ul class="menu vertical sublevel-1">
      <song v-for="song in playlist.songs" :song="song" :key="song.name" v-on:song-selected="setCurrentSong"></song>
    </ul>
  </li>`,
  props:['playlist'],
  mounted: function(){
    new Foundation.AccordionMenu($('#folders'));
  },
  methods: {
    setCurrentSong: function(song){
        this.playlist.songs.forEach(function(songo){
          songo.selected = false;
        });
        this.$emit('song-selected', {song: song, songList: this.playlist.songs});
    },
    showAddSongDialog: function(){
      let dis = this;
      dialog.showOpenDialog({
        properties: ['openFile'],
        filters: [{name: 'mp3', extensions: ['mp3']}],
        title: 'Select songs'
      },
      function (songs) {
        if(songs){
          songs.forEach(function(song){
            dis.addSong(song);
          });
          dis.$emit("update-playlist", dis.playlist);
        }
      });
    },
    addSong: function(songPath){
      let song = new Song(songPath);
      this.playlist.songs.push(song);
    }
  }
});
