
Vue.component('song', {
  template: `
  <li>
    <a class="subitem song" href="#"
       @click="setCurrentSong"
       :class="{ selected: song.selected }"
       v-bind:title="song.name">
       <i v-if="song.selected" class="fa fa-pause-circle" aria-hidden="true"></i>
       <i v-else class="fa fa-play-circle" aria-hidden="true"></i>
       {{song.name}}
    </a>
  </li>`,
  props:['song'],
  methods: {
    setCurrentSong: function() {
      console.log("currentSong " + this.song.name);
      this.$emit('song-selected', this.song);
    }
  }
});
