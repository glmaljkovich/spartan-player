const SongManager = require('../managers/song-manager.js');
const DBManager   = require('../managers/db-manager.js');
const DOMManager  = require('../managers/dom-manager.js');
const Song        = require('../models/song.js');
const SongList    = require('../models/songlist.js');

let AppComponent = Vue.extend({

  data: function(){
    return {
      currentSong: null,
      currentLocation: [],
      folders: [],
      playlists: [],
      dbManager: new DBManager(),
      domManager: new DOMManager(),
      songManager: new SongManager(),
      calls: 0
    };
  },
  created: function(){
    // initializeDB
    this.dbManager.initializeDB();
    this.folders      = this.dbManager.getFolders();
    this.playlists    = this.songManager.deserializePlaylists(this.dbManager.getPlayLists());

    this.domManager.setCoverImage("");
  },
  methods: {
    addFolder: function (folder) {
        this.dbManager.addFolder(folder);
        this.folders.push(folder);
        this.removeDuplicate(folder);
        console.log("[App Component] Agregado: " + folder.name);
    },
    removeDuplicate: function(folder){
      remove(this.folders, folder);
    },
    setCurrentSong: function(song){
      if (this.currentSong !== null) {
        this.currentSong.selected = false;
        this.currentSong.audio.pause();
      }
      this.currentSong = song;
      this.currentSong.selected = true;
      // Load audio
      this.loadSong();

    },
    setCurrentLocation: function(location){
      this.currentLocation = location;
    },
    setSongAndLocation: function(container){
      this.setCurrentSong(container.song);
      this.setCurrentLocation(container.songList);
    },
    loadSong: function(){
      let songPath = this.currentSong.path;
      this.currentSong.audio = new Audio(songPath);
    },
    nextSong: function(song){
      if(song)
        this.setCurrentSong(song);
    },
    previousSong: function(song){
      if(song)
        this.setCurrentSong(song);
    },
    updatePlayList: function(playlist){
      this.dbManager.updatePlayList(this.songManager.serializePlaylist(playlist));
    },
    addPlayList: function(playlist){
      this.dbManager.addPlayList(this.songManager.serializePlaylist(playlist));
      this.playlists.push(playlist);
      remove(this.playlists, playlist);
    }
  }
});

module.exports = AppComponent;
