const SongList    = require('../models/songlist.js');
const DOMManager  = require('../managers/dom-manager.js');

Vue.component('player', {
  template: `
  <div id="bar" class="small-12 columns">
    <!-- Cover -->
    <img src="img/cover.png" alt="" id="album-cover-small"/>
    <!-- Player -->
    <div id="player">
      <!-- Slider + Metadata -->
      <div class="small-10 medium-7 large-8 columns">
        <!-- Metadata -->
        <div class="small-8 medium-9 columns">
          <h1 class="song-name">--</h1>
          <h4 class="album-data">--</h4>
        </div>
        <div class="small-4 medium-3 columns">
          <h1 class="float-right">
            <a class="flow-control" href="#" @click="toggleShuffle" :class="{selected: shuffle }"><i class="fa fa-random" aria-hidden="true"></i></a>
          </h1>
          <h1 class="float-right">
            <a class="flow-control" href="#"><i class="fa fa-exchange" aria-hidden="true"></i></a>
          </h1>
        </div>
        <div class="small-12 columns">
          <!-- Slider -->
          <div id="slider" class="slider" data-initial-start="0" data-step="1" data-binding="true" >
            <span id="marker" class="slider-handle"  data-slider-handle role="slider" tabindex="1"></span>
            <span id="progress" class="slider-fill" data-slider-fill></span>
            <input id="playing-time" type="hidden">
          </div>
          <span id="position">00:00</span>
          <span id="duration" class="float-right">00:00</span>
        </div>
      </div>
      <!-- Controls -->
      <div class="small-12 medium-5 large-4 columns text-center">
        <div id="controls" class="">
          <a href="#" @click="previousSong" class="flow">
            <img src="img/next.svg" style="transform: rotate(180deg);">
          </a>
          <a id="play" @click="togglePlay" href="#">
            <img v-if="playing" src="img/pause.svg" style="padding: 12px;">
            <img v-else src="img/play.svg" style="padding: 12px 10px 12px 14px;">
          </a>
          <a href="#" @click="nextSong" class="flow">
            <img src="img/next.svg">
          </a>
        </div>
      </div>
    </div><!-- /#player-->
  </div><!-- /#bar-->`,
  props:['currentSong', 'currentLocation'],
  data: function(){
    return {
      playing: false,
      shuffle: false,
      domManager: new DOMManager()
    };
  },
  computed: {
    songList: function () {
      return new SongList(this.currentLocation);
    }
  },
  watch: {
    currentSong: function (song) {
      let dis = this;
      song.audio.addEventListener('timeupdate',function (){
          if(!moving){
            var currentTime = parseInt(song.audio.currentTime, 10);
            $("#playing-time").val(currentTime);
            $('#position').text(dis.domManager.toPlayTime(currentTime));
            $('#playing-time').change();
          }
      });
      this.domManager.trackSliderPosition(song.audio);
      this.domManager.loadMetadata(song);
      this.playing = true;
      song.audio.play();
    }
  },
  methods: {
    nextSong: function(){
      let next = this.songList.nextSong(this.currentSong);
      this.$emit('next-song', next);
    },
    previousSong: function(){
      let previous = this.songList.previousSong(this.currentSong);
      this.$emit('previous-song', previous);
    },
    togglePlay: function(){
      if(this.playing){
        this.playing = false;
        this.currentSong.audio.pause();
      } else{
        this.playing = true;
        this.currentSong.audio.play();
      }
    },
    toggleShuffle: function(){
      if(this.shuffle){
        this.songList.setOriginalOrder();
        this.shuffle = false;
      } else{
        this.songList.shuffle();
        this.shuffle = true;
      }
    },
  }
});
