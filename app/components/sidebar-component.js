  Vue.component('sidebar', {
    template: `
    <div class="small-10 medium-4 xlarge-2 off-canvas-absolute position-left" id="toolbar" data-off-canvas>
      <div class="relative columns">
        <img src="img/logo.png" alt="Spartan" class="text-center logo" />
        <!-- Folders -->
        <ul class="vertical accordion-menu menu" id="folders" data-accordion-menu>
          <p>FOLDERS</p>
          <slot name="folders">Folders</slot>
          <p>PLAYLISTS</p>
          <slot name="playlists">Play Lists</slot>
        </ul>
      </div>
      <add-menu v-on:add-folder="addFolder"></add-menu>
    </div>`,
    methods: {
      addFolder: function(folder) {
        console.log("[addMenu] AddFolder: " + folder);
        this.$emit('add-folder', folder);
      }
    }
  });
