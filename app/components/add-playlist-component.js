const PlayList = require('../models/playlist.js');

Vue.component('new-playlist-modal', {
  template: `
  <div class="reveal" id="add-playlist" data-reveal>
    <p class="lead">New Playlist</p>
    <button class="close-button" data-close aria-label="Close modal" type="button">
      <span aria-hidden="true">&times;</span>
    </button>
    <input id="list-name" type="text" name="list-name" :value="name" @input="update" placeholder="New Playlist">

    <button v-if="name != ''" type="button" name="button" @click="addPlayList">OK</button>
    <i v-else>Write a name for your playlist</i>
  </div>`,
  data: function(){
    return {name: ''};
  },
  methods: {
    addPlayList: function() {
      let playlist = new PlayList(this.name);
      this.$emit('add-playlist', playlist);
    },
    update: function(e){
      this.name = e.target.value;
    }
  }
});
