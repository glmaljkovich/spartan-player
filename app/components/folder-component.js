const Song = require("../models/song.js");
const f    = require("../lib/functions.js");

Vue.component('folder', {
  template: `<li class="song-container">
    <a href="#">{{folder.name}}</a>
    <ul class="menu vertical sublevel-1">
      <song v-for="song in songs" :song="song" :key="song.name" v-on:song-selected="setCurrentSong"></song>
    </ul>
  </li>`,
  props:['folder'],
  data: function(){
    return {songs: []};
  },
  mounted: function(){
    this.getFolderSongs(this.folder.path);
    new Foundation.AccordionMenu($('#folders'));
  },
  methods: {
    getFolderSongs: function(folder){
      var dis = this;
      fs.readdir(folder, function(err, files) {
        if (err) return;

        files.filter(f.isMP3)
             .forEach(function(sname){
                let song = new Song(dis.folder.path + "/" + sname);
                dis.songs.push(song);
              });
      });
    },
    setCurrentSong: function(song){
        this.songs.forEach(function(songo){
          songo.selected = false;
        });
        this.$emit('song-selected', {song: song, songList: this.songs});
    }
  }
});
