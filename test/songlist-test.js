const Node        = require('../app/models/linkedlist.js').Node;
const LinkedList  = require('../app/models/linkedlist.js').LinkedList;
const Song        = require('../app/models/song.js');
const SongList    = require('../app/models/songlist.js');
const PlayList    = require('../app/models/playlist.js');
const SongManager = require('../app/managers/song-manager.js');
const f           = require('../app/lib/functions.js');
const should      = require('should');

describe('SongList', function() {
  let manager, playlist, plist, songs, songList;

  beforeEach(function() {
    manager   = new SongManager();
    playlist  = new PlayList("Top 10");

    playlist.setSongs([
      "/home/gabriel/Música/Ed Sheeran - Shape Of You [Official Lyric Video](1).mp3",
      "/home/gabriel/Descargas/The Weeknd - I Feel It Coming ft. Daft Punk.mp3",
      "/home/gabriel/Descargas/ACDC - Back In Black.mp3"
    ]);

    plist     = manager.deserializePlaylist(playlist);
    songs     = plist.songs;
    songList  = new SongList(songs);
  });

  describe('#getNodeArray()', function() {

    it('should return an Array with the same length as the original', function() {
      let result = songList.getNodeArray(songs);
      result.length.should.equal(songs.length);
    });

    describe('should return an Array containing all the songs in the same order given', function() {
      let songNames = [
        "Ed Sheeran - Shape Of You [Official Lyric Video](1).mp3",
        "The Weeknd - I Feel It Coming ft. Daft Punk.mp3",
        "ACDC - Back In Black.mp3"
      ];

      songNames.forEach(function(song, position){
        it('contains ' + song, function() {
          let nodeArray = songList.getNodeArray(songs);

          nodeArray.some(function(songNode, index){
            return songNode.data.name == song && position == index;
          }).should.be.true();
        });
      });

    });
  });

  describe('#findSong()', function(){
    it('should return a song for an existing key', function() {
      songList.findSong(songs[0]).should.not.be.null();
    });

    it('should return the correct song', function() {
      let song = songs[0];
      songList.findSong(song).data.should.equal(song);
    });

    it('should return null for an non-existent song', function() {
      let song = new Song('/home/music/truchinga/Trucha.mp3');
      should(songList.findSong(song)).be.null();
    });
  });

  describe('#nextSong()', function(){
    it('should return a song for the first list element', function() {
      let song = songs[0];
      songList.nextSong(song).should.equal(songs[1]);
    });

    it('should return a song for the second list element', function() {
      let song = songs[1];
      songList.nextSong(song).should.equal(songs[2]);
    });

    it('should return null for the last element of the list', function() {
      let song = songs[2];
      should(songList.nextSong(song)).be.null();
    });
  });

  describe('#previousSong()', function(){
    it('should return a song for the last list element', function() {
      let song = songs[2];
      songList.previousSong(song).should.equal(songs[1]);
    });

    it('should return a song for the second list element', function() {
      let song = songs[1];
      songList.previousSong(song).should.equal(songs[0]);
    });

    it('should return null for the first element of the list', function() {
      let song = songs[0];
      should(songList.previousSong(song)).be.null();
    });
  });

});
