const DBManager = require('../app/managers/db-manager.js');

describe('DBManager', function() {
  let manager = new DBManager();

  describe('#serializeSong()', function() {
    let songPath = "/home/gabriel/Descargas/The Weeknd - I Feel It Coming ft. Daft Punk.mp3";
    let song = manager.deserializeSong(songPath);

    it('should return the absolute path', function() {
      manager.serializeSong(song).should.equal(songPath);
    });
  });



});
