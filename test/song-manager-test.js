
const SongManager = require('../app/managers/song-manager.js');
const should      = require('should');
const PlayList    = require('../app/models/playlist.js');
const Song        = require('../app/models/song.js');


describe('SongManager', function() {
  let manager = new SongManager();

  describe('#deserializeSong()', function() {
    let song = "/home/gabriel/Descargas/The Weeknd - I Feel It Coming ft. Daft Punk.mp3";

    it('should set correct name', function() {
      manager.deserializeSong(song).name.should.equal("The Weeknd - I Feel It Coming ft. Daft Punk.mp3");
    });

    it('should set correct folder name', function() {
      manager.deserializeSong(song).folder.name.should.equal("Descargas");
    });

    it('should set correct folder path', function() {
      manager.deserializeSong(song).folder.path.should.equal("/home/gabriel/Descargas");
    });
  });

  describe('#serializeSong()', function() {
    let songPath = "/home/gabriel/Descargas/The Weeknd - I Feel It Coming ft. Daft Punk.mp3";
    let song = manager.deserializeSong(songPath);

    it('should return the absolute path', function() {
      manager.serializeSong(song).should.equal(songPath);
    });
  });

  describe('#deserializePlaylist()', function() {
    let playlist = new PlayList("Top 10");

    playlist.setSongs([
      "/home/gabriel/Música/Ed Sheeran - Shape Of You [Official Lyric Video](1).mp3",
      "/home/gabriel/Descargas/The Weeknd - I Feel It Coming ft. Daft Punk.mp3",
      "/home/gabriel/Descargas/ACDC - Back In Black.mp3"
    ]);

    it('should create three songs', function() {
      let plist = manager.deserializePlaylist(playlist);
      let songs = plist.songs;

      songs.should.have.length(3);
    });

    it('should create three song objects', function() {
      let plist = manager.deserializePlaylist(playlist);
      let songs = plist.songs;

      songs.should.matchEach(function(song){
        song.should.be.instanceOf(Song);
      });
    });
  });
});
