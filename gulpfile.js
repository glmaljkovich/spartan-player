var gulp = require('gulp');
var $    = require('gulp-load-plugins')();
var concat = require('gulp-concat');

var sassPaths = [
  'css',
  'bower_components/normalize.scss/sass',
  'bower_components/foundation-sites/scss',
  'bower_components/motion-ui/src'
];

gulp.task('sass', function() {
  return gulp.src('css/app.scss')
    .pipe($.sass({
      includePaths: sassPaths,
      outputStyle: 'compressed' // if css compressed **file size**
    })
      .on('error', $.sass.logError))
    .pipe($.autoprefixer({
      browsers: ['last 2 versions', 'ie >= 9']
    }))
    .pipe(gulp.dest('css'));
});

gulp.task('scripts', function() {
  return gulp.src(['./app/lib/canvas-image-cover.js',
                   './app/lib/stackblur.js',
                   './app/lib/moment.js',
                   './app/lib/vue.js',
                   './app/lib/setup.js'])
    .pipe(concat('app.js'))
    .pipe(gulp.dest('./app/'));
});

gulp.task('default', ['sass', 'scripts'], function() {
  // gulp.watch(['css/*.scss'], ['sass']);
});
